# encoding=utf8
import re
import os
import sys
import urllib2
from bs4 import BeautifulSoup as soup
import subprocess
from contextlib import contextmanager
import time

# https://www.loc.gov/collections/japanese-fine-prints-pre-1915/?c=25&sp=2&st=list

numpage = 25
locjapan_url = 'https://www.loc.gov/collections/japanese-fine-prints-pre-1915/?st=list&c={numpage}'
locjapan_url_page = 'https://www.loc.gov/collections/japanese-fine-prints-pre-1915/?c={numpage}&sp={page}&st=list'
# option value="https://cdn.loc.gov/service/pnp/cph/3g10000/3g10000/3g10500/3g10542v.jpg"
#                            data-file-download="JPEG"
#                            >JPEG&nbsp;(159.5 KB)
#                        </option>
#'
#link_to_image = <a href="https://lccn.loc.gov/2002700266"
#       target="_blank">https://lccn.loc.gov/2002700266</a>

image_extensions = ['jpg', 'jpeg'] # 'gif', 'jpeg', 'tif']

def get_image_page(img_page_url, dest_dir):
    print "Processing {}".format(img_page_url)
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
    page = opener.open(img_page_url)
    data = soup(page.read(), "html.parser")
    links = data.find_all("link")
    links = [x for x in links if 'rel' in x.attrs.keys()]
    links = [x.attrs['href'] for x in links if 'alternate' in x.attrs['rel']]
    links = [x for x in links if any(ext in x for ext in image_extensions)]
    with pushd(dest_dir):
        for link in links:
            file = link.split('/')[-1]
	    if not os.path.exists(file):
                cmd = "curl -o {localfile} {url}".format(localfile=file, url=link)
                result = get_command_output(cmd)
                if result['errorcode'] <> 0:
                    print result
	    else:
		print "File {} already downloaded".format(file)
        pass


def fetch_a_page(numer_per_page, pagenum, dest_dir):
    url = locjapan_url_page.format(numpage=numer_per_page, page=pagenum)
    print "Processing page {} : {}".format(pagenum, url)
    opener = urllib2.build_opener()
    opener.addheaders = [('User-Agent', 'Mozilla/5.0')]
    page = opener.open(url)
    data = soup(page.read(), "html.parser")
    hrefs = data.find_all('a')
    hrefs = [x for x in hrefs if 'href' in x.attrs.keys()]
    hrefs = [x for x in hrefs if 'rel' in x.attrs.keys()]
    hrefs = [x for x in hrefs if 'aria-hidden' not in x.attrs.keys()]
    for href in hrefs:
        img_page_url = href.attrs['href']
        img_page_rel = href.attrs['rel'][0]
        img_text = href.contents[0].replace('\n', '').strip()
        # now need to download the images
        get_image_page(img_page_url, dest_dir)
        print "Sleeping..."
        time.sleep(4)
    pass


def main():
    if not os.path.exists('./jpg'):
        os.mkdir('./jpg')
    for page in range(82, 100):
        try:
            print("Page {} --------------------------------".format(page))
            fetch_a_page(numpage, page, './jpg')
	    print("Sleeping/page")
	    time.sleep(30)
        except Exception as e:
            print "Processing halted at page {page} exception {e}".format(page=page, e=e)
            sys.exit(1)
    sys.exit(0)


@contextmanager
def pushd(new_dir):
    """ this is to handle changing directory for a block and restoring the directory
    @param new_dir: directory to cd to and put on the call stack
    when the block has exited."""
    # ref: http://stackoverflow.com/questions/6194499/python-os-system-pushddef
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    yield
    os.chdir(previous_dir)


def get_command_output(command):
    """
    Execute a shell command and return its output..
    @param command: The command line to execute, list first element is program, remaining elements are args
    @return: dictionary of errorcode, stdout, and stderr data
    """
    print command
    sp = subprocess.Popen(command, stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE,
                          shell=True)
    out, err = sp.communicate()
    returnvalue = dict(errorcode=sp.returncode, stdout=out, stderr=err)
    return returnvalue


main()
